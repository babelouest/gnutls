/*
 * Copyright (C) 2021 Free Software Foundation, Inc.
 *
 * Author: Nicolas Mora
 *
 * This file is part of GnuTLS.
 *
 * The GnuTLS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.	If not, see <https://www.gnu.org/licenses/>
 *
 */

#include "gnutls_int.h"
#include "abstract_int.h"
#include "pk.h"
#include <ecc-compute.h>
#include "errors.h"

/**
 * gnutls_privkey_compute_shared_secret:
 * @privkey: is a #gnutls_privkey_t key to compute
 * @pubkey: is a #gnutls_pubkey_t key to compute with privkey
 * @secret: the computation result
 *
 * This function calculates a key agreement with Elliptic
 * Curve Diffie-Hellman Ephemeral Static using a private and
 * a public ECC keys
 *
 * Returns: On success, %GNUTLS_E_SUCCESS (0) is returned, otherwise a
 *	 negative error value.
 *
 * On success, secret->data must be gnutls_free'd after use
 *
 * Since: 3.7.3
 **/
int
gnutls_privkey_compute_shared_secret(gnutls_privkey_t privkey, gnutls_pubkey_t pubkey, gnutls_datum_t *secret)
{
	uint privkey_bits = 0, pubkey_bits = 0;
	gnutls_pk_algorithm_t privkey_alg = gnutls_privkey_get_pk_algorithm(privkey, &privkey_bits), pubkey_alg = gnutls_pubkey_get_pk_algorithm(pubkey, &pubkey_bits);
	gnutls_pk_algorithm_t alg = GNUTLS_PK_UNKNOWN;
	gnutls_datum_t priv_k = {NULL, 0}, pub_x = {NULL, 0};
	gnutls_pk_params_st privkey_params, pubkey_params;
	int ret;
	unsigned int params_nr;

	secret->data = NULL;
	secret->size = 0;

	if (privkey_alg != pubkey_alg || privkey_bits != pubkey_bits)
	{
		return GNUTLS_E_INVALID_REQUEST;
	}
	
	if (privkey_alg == GNUTLS_PK_ECDSA)
	{
		alg = GNUTLS_PK_ECDSA;
		params_nr = 3;
	}
	else if (privkey_alg == GNUTLS_PK_ECDH_X25519 || privkey_alg == GNUTLS_PK_ECDH_X448)
	{
		alg = GNUTLS_PK_DH;
		params_nr = 2;
	}
	else
	{
		return GNUTLS_E_INVALID_REQUEST;
	}

	gnutls_pk_params_init(&privkey_params);
	gnutls_pk_params_init(&pubkey_params);

	if ((ret = _gnutls_privkey_get_mpis(privkey, &privkey_params)) != GNUTLS_E_SUCCESS)
	{
		goto cleanup;
	}
	
	if (_gnutls_mpi_init_scan_nz (&privkey_params.params[ECC_K], privkey_params.raw_priv.data, privkey_params.raw_priv.size) != 0) {
		ret = gnutls_assert_val(GNUTLS_E_MPI_SCAN_FAILED);
		goto cleanup;
	}

	privkey_params.params_nr = params_nr;
	privkey_params.algo = alg;

	if ((ret = _gnutls_pubkey_get_mpis(pubkey, &pubkey_params)) != GNUTLS_E_SUCCESS)
	{
		goto cleanup;
	}
	
	pubkey_params.params_nr = params_nr;
	pubkey_params.algo = alg;

	if ((ret = _gnutls_pk_derive(alg, secret, &pubkey_params, &privkey_params)) < 0)
	{
		gnutls_free(secret->data);
	}

	cleanup:
		gnutls_pk_params_release(&privkey_params);
		gnutls_pk_params_release(&pubkey_params);
		zeroize_key(priv_k.data, priv_k.size);
		zeroize_key(pub_x.data, pub_x.size);
		gnutls_free(priv_k.data);
		gnutls_free(pub_x.data);

	return ret;
}
