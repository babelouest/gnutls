/*
 * Copyright (C) 2021 Free Software Foundation, Inc.
 *
 * Author: Nicolas Mora
 *
 * This file is part of GnuTLS.
 *
 * The GnuTLS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 *
 */

#ifndef GNUTLS_LIB_ECC_COMPUTE_H
#define GNUTLS_LIB_ECC_COMPUTE_H

int gnutls_privkey_compute_shared_secret(gnutls_privkey_t privkey, gnutls_pubkey_t pubkey, gnutls_datum_t *Z);

#endif /* GNUTLS_LIB_ECC_COMPUTE_H */
