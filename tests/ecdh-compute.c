/*
 * Copyright (C) 2019 Red Hat, Inc.
 *
 * Author: Simo Sorce
 *
 * This file is part of GnuTLS.
 *
 * GnuTLS is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * GnuTLS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GnuTLS; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

/* This program tests functionality of DH exchanges */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnutls/gnutls.h>
#include <gnutls/x509.h>
#include <gnutls/abstract.h>
#include <gnutls/crypto.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include "utils.h"

const unsigned char private_key_1_ec256[] = 
"-----BEGIN EC PRIVATE KEY-----\n"
"MHcCAQEEIHyh0dDemPadDGy++p6zTeVpvU9r1XNDhjMMDzwU6YpQoAoGCCqGSM49\n"
"AwEHoUQDQgAEo4K2IteQZ1nKmLMCGd9RkgikfrnGqkk4yGAS+/UfY2r8LtJ1vnep\n"
"xUWw1XYG6A5KNuUcAAVVwpQ0VcyQ9aAidA==\n"
"-----END EC PRIVATE KEY-----";
const unsigned char public_key_1_ec256[] = 
"-----BEGIN PUBLIC KEY-----\n"
"MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEo4K2IteQZ1nKmLMCGd9RkgikfrnG\n"
"qkk4yGAS+/UfY2r8LtJ1vnepxUWw1XYG6A5KNuUcAAVVwpQ0VcyQ9aAidA==\n"
"-----END PUBLIC KEY-----";
const unsigned char private_key_2_ec256[] = 
"-----BEGIN EC PRIVATE KEY-----\n"
"MHgCAQEEIQDV8Ou26xlaco1lUt7wa3Xq4plhijZiGYxWKXYr6bITtaAKBggqhkjO\n"
"PQMBB6FEA0IABOgkbcUgixTM9ejDv4SLIyubBQmllz0RIGDcN1H7QmgpsTN5CA4U\n"
"b4EwPRw2w1Q5lt+0aQpK+3i0i6zMvAtf3Pk=\n"
"-----END EC PRIVATE KEY-----";
const unsigned char public_key_2_ec256[] = 
"-----BEGIN PUBLIC KEY-----\n"
"MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE6CRtxSCLFMz16MO/hIsjK5sFCaWX\n"
"PREgYNw3UftCaCmxM3kIDhRvgTA9HDbDVDmW37RpCkr7eLSLrMy8C1/c+Q==\n"
"-----END PUBLIC KEY-----";
const unsigned char private_key_3_ec256[] = 
"-----BEGIN EC PRIVATE KEY-----\n"
"MHcCAQEEIDSapWd9STPxrE1v40jlaDCkD/UVYYNA5zzFEYX/FJ+SoAoGCCqGSM49\n"
"AwEHoUQDQgAE1XZxTDjF6y9FB7d6zJ8iDFnXVP8zQryCyCjOVAvHIdLfWNDNQ5u8\n"
"1lRo8M0DBzL4l9k6ZOcqpgoxJDesFyEL4g==\n"
"-----END EC PRIVATE KEY-----";
const unsigned char public_key_3_ec256[] = 
"-----BEGIN PUBLIC KEY-----\n"
"MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE1XZxTDjF6y9FB7d6zJ8iDFnXVP8z\n"
"QryCyCjOVAvHIdLfWNDNQ5u81lRo8M0DBzL4l9k6ZOcqpgoxJDesFyEL4g==\n"
"-----END PUBLIC KEY-----";
const unsigned char private_key_1_ec384[] = 
"-----BEGIN EC PRIVATE KEY-----\n"
"MIGlAgEBBDEA0lUelelGTbn2Ea+jhi5DZQntWhilStuadk5Pg6eNooAovTR161hi\n"
"uiFuupcB7WsQoAcGBSuBBAAioWQDYgAEG9ImwzC8FDeeMJ/RZSQhtODDWqXVEYDE\n"
"hmwwXBnH2VetVBbqPb0u/BZYSBQMLKIY46XbL7l2hfGya536E3qURGCqOM/G5nK+\n"
"qz+0YuizySW60jk5yxT6Bb3iw5rg5STs\n"
"-----END EC PRIVATE KEY-----";
const unsigned char public_key_1_ec384[] = 
"-----BEGIN PUBLIC KEY-----\n"
"MHYwEAYHKoZIzj0CAQYFK4EEACIDYgAEG9ImwzC8FDeeMJ/RZSQhtODDWqXVEYDE\n"
"hmwwXBnH2VetVBbqPb0u/BZYSBQMLKIY46XbL7l2hfGya536E3qURGCqOM/G5nK+\n"
"qz+0YuizySW60jk5yxT6Bb3iw5rg5STs\n"
"-----END PUBLIC KEY-----";
const unsigned char private_key_2_ec384[] = 
"-----BEGIN EC PRIVATE KEY-----\n"
"MIGkAgEBBDBTHzEW4fseP+mBnaP6grDgMkQ7WcAIjySKrkqPY1Vafc1etsa4YroS\n"
"ySf2gBf+NZCgBwYFK4EEACKhZANiAARsN918ZCQejvdN+u3foCAOSguHD6/LlTkl\n"
"EeIjUascNYVnbyEhPKG6py4qFdciwtD/XNMTl1xf0iK9eDqrTLpecK0WQ/WB2pJ9\n"
"OosderyBZLXVKDLHXko10jvEOVWz0Lk=\n"
"-----END EC PRIVATE KEY-----";
const unsigned char public_key_2_ec384[] = 
"-----BEGIN PUBLIC KEY-----\n"
"MHYwEAYHKoZIzj0CAQYFK4EEACIDYgAEbDfdfGQkHo73Tfrt36AgDkoLhw+vy5U5\n"
"JRHiI1GrHDWFZ28hITyhuqcuKhXXIsLQ/1zTE5dcX9IivXg6q0y6XnCtFkP1gdqS\n"
"fTqLHXq8gWS11Sgyx15KNdI7xDlVs9C5\n"
"-----END PUBLIC KEY-----";
const unsigned char private_key_1_ec521[] = 
"-----BEGIN EC PRIVATE KEY-----\n"
"MIHcAgEBBEIBpLBCVTeFJ0/PH/jFR80MgSPa6DxI1HNkIyIS+t+3boL+x8G/nTHq\n"
"RTCn+0YUulJl477j+HX4VA132giSrV4E1sWgBwYFK4EEACOhgYkDgYYABABr8H2M\n"
"XCQ+UzYLB2jpigCwZ/PQRcIPz6mIl7UItxqmlRojgGx+dNvAkTKj7aBf4CDtuZq6\n"
"cRl8cdnZ27YsbJ0erAEBbQ582tk9A9VRMO8j8hXSguZ01oO00r0rVTPdv7X/arV4\n"
"PMOnl6RitI/ry/V03bbdj2FxccnLvfZkg97tuNyklw==\n"
"-----END EC PRIVATE KEY-----";
const unsigned char public_key_1_ec521[] = 
"-----BEGIN PUBLIC KEY-----\n"
"MIGbMBAGByqGSM49AgEGBSuBBAAjA4GGAAQAa/B9jFwkPlM2Cwdo6YoAsGfz0EXC\n"
"D8+piJe1CLcappUaI4BsfnTbwJEyo+2gX+Ag7bmaunEZfHHZ2du2LGydHqwBAW0O\n"
"fNrZPQPVUTDvI/IV0oLmdNaDtNK9K1Uz3b+1/2q1eDzDp5ekYrSP68v1dN223Y9h\n"
"cXHJy732ZIPe7bjcpJc=\n"
"-----END PUBLIC KEY-----";
const unsigned char private_key_2_ec521[] = 
"-----BEGIN EC PRIVATE KEY-----\n"
"MIHbAgEBBEF03JES/JR1FDvDKZcfggsVmeChru5ps4ZyJgPQbgcgYtPiDcvK52oF\n"
"4oszn7DLN/EAabdOyokxATqXFQokNpJcoaAHBgUrgQQAI6GBiQOBhgAEAYf/UjDy\n"
"TF219mC1AoeZd9B/XQQs3vEJ5C7tkLbNpciAUgg2T6xYMGcD1A+ip0viHNbxTCTw\n"
"yAMbtdpsZcCmEeZ4AOXZD5W0iPt8gYx7C4bbHmJgVy0cSdGVZW+rzDb1CbLjK2oI\n"
"kFZ7H8tglNCbH8xO4pStBRTjoH3+jAAPQRGeNgCN\n"
"-----END EC PRIVATE KEY-----";
const unsigned char public_key_2_ec521[] = 
"-----BEGIN PUBLIC KEY-----\n"
"MIGbMBAGByqGSM49AgEGBSuBBAAjA4GGAAQBh/9SMPJMXbX2YLUCh5l30H9dBCze\n"
"8QnkLu2Qts2lyIBSCDZPrFgwZwPUD6KnS+Ic1vFMJPDIAxu12mxlwKYR5ngA5dkP\n"
"lbSI+3yBjHsLhtseYmBXLRxJ0ZVlb6vMNvUJsuMragiQVnsfy2CU0JsfzE7ilK0F\n"
"FOOgff6MAA9BEZ42AI0="
"-----END PUBLIC KEY-----";
const unsigned char private_key_rsa[] = 
"-----BEGIN RSA PRIVATE KEY-----\n"
"MIIEowIBAAKCAQEAzM2Rce1zJSpPQuIcKyDNbBrTy6oBLsv0+mEbIhN/pUebWQxy\n"
"HjctguPSNrBqViwEDuAMB0ZkU5ZVoFkHG0wqQhMSXbpfvmFBHHL9D5OPK01OPVy7\n"
"yU/oCfYnVCYD1S+COdVlOP+ekVIPZUjZso5iSzbwHOf7LdKzdFrYLG1LM2c+Ji9N\n"
"OnTM/1MxQv4HamCOi/QTCqpH6V+wy7oaU+4gbGJHn3El6ely2RSGk7BRRfSexHa0\n"
"wiQFPlY9PzAZjZmBKWvjQ8/FhceUBAW98SoX5XE/8Nape8UKckesHXKIhV5y2hJw\n"
"+oEeG+RVwyXeeN6PtLuLoUTOzCteQR9dwG9smQIDAQABAoIBACFD/qznqb1OqwMt\n"
"IPAsatF1ZhN9sKV+QXNt1G9rhV8+S1BigiLASacqBBoX+521lo8n+ywvVdpx38Sk\n"
"0U+wxmhnGqOYwzZ0K5RIZSxjhmy9KMk0x9fCH+mGTwsPBa5aAigMrl1cv8/Q6WlF\n"
"NP8Yu4SGDuhTEex2femXW+eTdRSLFLb9viS74PmEbAm8SbHesF6dIb7zvenxWyAf\n"
"qFoPVbflHX8zveHV5saals4ONQQIZ5BBL/rPubT8TXTRNXXtyRzbR68BecmdEtS4\n"
"qLnmYwIxK5Rz3lI1QhQImp47FiuFD4h4BQDnkz0iRzk0SdOqt0mjSPJo/oRuvP7M\n"
"5VHmbjUCgYEAzmUT44Aze2T7bu81m9jVukJI7D1BMzMk/Vm0dLd1fqY/SIZh3Cr/\n"
"YlAjHHvBSaB79aOH+Y5OGPzG9vl5ap9r3hbs8kVzRUv0CLIcVl+IRRhbfqtYcfOn\n"
"W+jfYisgtRqOOE8WuhrNLO2j3H8vi8sqRJM3A1qyMdpMuALu+datjbMCgYEA/gaM\n"
"lWmw42zz/blhpiv5i11HxehVXTPgx9nxPfesB5ddnj7b2HY4R+IbfXYb1Mb8Pdrc\n"
"NUAbjk3fWLcECrKOJN9bywN62wIVnqkBQBE1Y653bD5pcbQ7TZa88v1pyvxNqoy5\n"
"vKvjREzs/dXOYIggfYdf9smXnfHQw04WgsXPboMCgYEAqVv1qvuawwlE7G2rbrvJ\n"
"/THBBuDorGuuUZoXYqLb7dXbHdbvXCuILhMWDJxcKd9feFv9CsFDwmtBlI13nQ8+\n"
"AxqKye5FPTqq/6uvZwyzFAd9pH5TcAYAcYGwM6yyBQnKY9g6DPJLJC/IAvn2eN54\n"
"S5cFUGJO0GuSAnidwjLvbNMCgYAUod2LSTFX14Kdyg/XWl0DZ8krlupQDqWkNdx1\n"
"RweO+kldUAe1NizuEbxf3Vb3Wk4kjAE0xgc28LZCU2Mdp1EKR6YCAD9bSHkPEPUM\n"
"ChnHBeWnQg54cMYcUDD6n4CXPxTj1FhFrcRgyMq690Gy73uMFpHBzq/k4T4ujsof\n"
"RwfJFwKBgBfe0N/mcXGfDSxwK+oRNQB0to0kNSXTownE5nHh8Ib2T56LWKlbKKyc\n"
"ujBok40Zmr7I00wDExzEHOP2r9J4uvvO53Bn1iHWgTMulFwote+Wn3tj6TQgq0Wd\n"
"1peoI/wQu02REUTOJ9amhgnQlSwa1eg/rLa6K29fUBhTuiYe+Pk8\n"
"-----END RSA PRIVATE KEY-----";
const unsigned char public_key_rsa[] = 
"-----BEGIN PUBLIC KEY-----\n"
"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzM2Rce1zJSpPQuIcKyDN\n"
"bBrTy6oBLsv0+mEbIhN/pUebWQxyHjctguPSNrBqViwEDuAMB0ZkU5ZVoFkHG0wq\n"
"QhMSXbpfvmFBHHL9D5OPK01OPVy7yU/oCfYnVCYD1S+COdVlOP+ekVIPZUjZso5i\n"
"SzbwHOf7LdKzdFrYLG1LM2c+Ji9NOnTM/1MxQv4HamCOi/QTCqpH6V+wy7oaU+4g\n"
"bGJHn3El6ely2RSGk7BRRfSexHa0wiQFPlY9PzAZjZmBKWvjQ8/FhceUBAW98SoX\n"
"5XE/8Nape8UKckesHXKIhV5y2hJw+oEeG+RVwyXeeN6PtLuLoUTOzCteQR9dwG9s\n"
"mQIDAQAB\n"
"-----END PUBLIC KEY-----";


int _gnutls_ecdh_compute_key(gnutls_ecc_curve_t curve,
				 const gnutls_datum_t *x, const gnutls_datum_t *y,
				 const gnutls_datum_t *k,
				 const gnutls_datum_t *peer_x, const gnutls_datum_t *peer_y,
				 gnutls_datum_t *Z);

int _gnutls_ecdh_generate_key(gnutls_ecc_curve_t curve,
						gnutls_datum_t *x, gnutls_datum_t *y,
						gnutls_datum_t *k);

static void genkey(gnutls_ecc_curve_t curve, gnutls_datum_t *x,
									 gnutls_datum_t *y, gnutls_datum_t *key)
{
	int ret;

	ret = _gnutls_ecdh_generate_key(curve, x, y, key);
	if (ret != 0)
		fail("error\n");
}

static void compute_key(gnutls_ecc_curve_t curve, const gnutls_datum_t *x,
			const gnutls_datum_t *y, const gnutls_datum_t *key,
			const gnutls_datum_t *peer_x,
			const gnutls_datum_t *peer_y,
												int expect_error,
			gnutls_datum_t *result, bool expect_success)
{
	gnutls_datum_t Z = { 0 };
	bool success;
	int ret;

	ret = _gnutls_ecdh_compute_key(curve, x, y, key, peer_x, peer_y, &Z);
	if (expect_error != ret)
		fail("error (%d)\n", ret);

	if (result) {
		success = (Z.size != result->size &&
				 memcmp(Z.data, result->data, Z.size));
		if (success != expect_success)
			fail("error\n");
	}
	gnutls_free(Z.data);
}

struct dh_test_data {
	gnutls_ecc_curve_t curve;
	const gnutls_datum_t x;
	const gnutls_datum_t y;
	const gnutls_datum_t key;
	const gnutls_datum_t peer_x;
	const gnutls_datum_t peer_y;
	int expected_error;
};

static void gnutls_ecdh_compute(const unsigned char *str_privkey, size_t str_privkey_size, const unsigned char *str_pubkey, size_t str_pubkey_size, int expected_error)
{
	gnutls_x509_privkey_t x509_key;
	gnutls_privkey_t privkey;
	gnutls_pubkey_t pubkey;
	gnutls_datum_t Z = {NULL, 0}, data;
	int res;
	
	if (gnutls_x509_privkey_init(&x509_key))
		fail("error\n");

	if (gnutls_privkey_init(&privkey))
		fail("error\n");

	if (gnutls_pubkey_init(&pubkey))
		fail("error\n");
	
	data.data = (unsigned char *)str_privkey;
	data.size = str_privkey_size;
	if (gnutls_x509_privkey_import(x509_key, &data, GNUTLS_X509_FMT_PEM))
		fail("error\n");

	if (gnutls_privkey_import_x509(privkey, x509_key, 0))
		fail("error\n");

	data.data = (unsigned char *)str_pubkey;
	data.size = str_pubkey_size;
	if (gnutls_pubkey_import(pubkey, &data, GNUTLS_X509_FMT_PEM))
		fail("error\n");

	res = gnutls_privkey_compute_shared_secret(privkey, pubkey, &Z);
	gnutls_free(Z.data);
	
	if (res != expected_error)
		fail("error\n");
	
	gnutls_x509_privkey_deinit(x509_key);
	gnutls_privkey_deinit(privkey);
	gnutls_pubkey_deinit(pubkey);
}

static void verify_computation(const unsigned char *str_privkey, size_t str_privkey_size,
																const unsigned char *str_pubkey,size_t str_pubkey_size, 
																const unsigned char *computation, size_t computation_size)
{
	gnutls_x509_privkey_t x509_key;
	gnutls_privkey_t privkey;
	gnutls_pubkey_t pubkey;
	gnutls_datum_t Z = {NULL, 0}, data;
	
	if (gnutls_x509_privkey_init(&x509_key))
		fail("error\n");

	if (gnutls_privkey_init(&privkey))
		fail("error\n");

	if (gnutls_pubkey_init(&pubkey))
		fail("error\n");
	
	data.data = (unsigned char *)str_privkey;
	data.size = str_privkey_size;
	if (gnutls_x509_privkey_import(x509_key, &data, GNUTLS_X509_FMT_PEM))
		fail("error\n");

	if (gnutls_privkey_import_x509(privkey, x509_key, 0))
		fail("error\n");

	data.data = (unsigned char *)str_pubkey;
	data.size = str_pubkey_size;
	if (gnutls_pubkey_import(pubkey, &data, GNUTLS_X509_FMT_PEM))
		fail("error\n");

	if (gnutls_privkey_compute_shared_secret(privkey, pubkey, &Z))
		fail("error\n");

	if (Z.size != computation_size)
		fail("error\n");

	if (memcmp(Z.data, computation, computation_size))
		fail("error\n");

	gnutls_free(Z.data);
	
	gnutls_x509_privkey_deinit(x509_key);
	gnutls_privkey_deinit(privkey);
	gnutls_pubkey_deinit(pubkey);
}

void doit(void)
{
	struct dh_test_data test_data[] = {
		{
												/* x == 0, y == 0 */
			GNUTLS_ECC_CURVE_SECP256R1,
												{ 0 }, { 0 }, { 0 },
			{ (void *)"\x00", 1 },
			{ (void *)"\x00", 1 },
			/* Should be GNUTLS_E_PK_INVALID_PUBKEY but mpi scan
												 * balks on values of 0 */
												GNUTLS_E_MPI_SCAN_FAILED,
		},
		{
												/* x > p -1 */
			GNUTLS_ECC_CURVE_SECP256R1,
												{ 0 }, { 0 }, { 0 },
			{ (void *)"\xff\xff\xff\xff\x00\x00\x00\x01"
																	"\x00\x00\x00\x00\x00\x00\x00\x00"
																	"\x00\x00\x00\x00\xff\xff\xff\xff"
																	"\xff\xff\xff\xff\xff\xff\xff\xff", 1 },
			{ (void *)"\x02", 1 },
			GNUTLS_E_PK_INVALID_PUBKEY,
		},
		{
												/* y > p -1 */
			GNUTLS_ECC_CURVE_SECP256R1,
												{ 0 }, { 0 }, { 0 },
			{ (void *)"\x02", 1 },
			{ (void *)"\xff\xff\xff\xff\x00\x00\x00\x01"
																	"\x00\x00\x00\x00\x00\x00\x00\x00"
																	"\x00\x00\x00\x00\xff\xff\xff\xff"
																	"\xff\xff\xff\xff\xff\xff\xff\xff", 1 },
			GNUTLS_E_PK_INVALID_PUBKEY,
		},
		{
												/* From CAVS tests */
			GNUTLS_ECC_CURVE_SECP521R1,
												{ (void *)"\xac\xbe\x4a\xd4\xf6\x73\x44\x0a"
																	"\xfc\x31\xf0\xb0\x3d\x28\xd4\xd5"
																	"\x14\xbe\x7b\xdd\x7a\x31\xb0\x32"
																	"\xec\x27\x27\x17\xa5\x7d\xc2\x6c"
																	"\xc4\xc9\x56\x29\xdb\x2d\x8c\x05"
																	"\x86\x2b\xe6\x15\xc6\x06\x28\xa3"
																	"\x24\xf2\x01\x7f\x98\xbd\xf9\x11"
																	"\xcc\xf8\x83\x5e\x43\x9e\xb2\xc1"
																	"\x88", 65 },
												{ (void *)"\xd6\x9b\x29\xa2\x37\x82\x36\x92"
																	"\xe8\xdb\x90\xa3\x25\x68\x67\x6c"
																	"\x92\xff\x3d\x23\x85\xe2\xfd\x13"
																	"\x16\x12\x72\xb3\x4b\x55\x88\x72"
																	"\xb0\x35\xab\xb5\x10\x89\x52\x5f"
																	"\x42\x9f\x53\x02\x60\x80\xc3\xd5"
																	"\x36\x6e\xe9\xdd\x28\xae\xd2\x38"
																	"\xab\xbe\x68\x6a\x54\x3e\x19\xf2"
																	"\x77", 65 },
												{ (void *)"\xd7\xdd\x17\x7c\xb9\x7f\x19\x09"
																	"\xbe\x56\x79\xba\x38\x7b\xee\x64"
																	"\xf7\xb4\x08\x4a\x4f\xaa\x6c\x31"
																	"\x8b\x82\xe9\xf2\xf7\x50\xc5\xc1"
																	"\x82\x26\x20\xd4\x88\x25\x0b\xf6"
																	"\xb4\x14\xea\x9b\x2c\x07\x93\x50"
																	"\xb9\xad\x78\x0a\x5e\xc6\xa6\xf8"
																	"\xb2\x9f\xa1\xc4\x76\xce\x1d\xa9"
																	"\xf5", 65 },
												{ (void *)"\x01\x41\xbe\x1a\xfa\x21\x99\xc9"
																	"\xb2\x2d\xaa\x0a\xff\x90\xb2\x67"
																	"\x18\xa2\x67\x04\x7e\xae\x28\x40"
																	"\xe8\xbc\xa0\xbd\x0c\x75\x41\x51"
																	"\xf1\xa0\x4d\xcf\x09\xa5\x4f\x1e"
																	"\x13\x5e\xa0\xdd\x13\xed\x86\x74"
																	"\x05\xc0\xcb\x6d\xac\x14\x6a\x24"
																	"\xb8\xdc\xf3\x78\xed\xed\x5d\xcd"
																	"\x57\x5b", 66 },
												{ (void *)"\x19\x52\xbd\x5d\xe6\x26\x40\xc3"
																	"\xfc\x8c\xc1\x55\xe2\x9c\x71\x14"
																	"\x5e\xdc\x62\x1c\x3a\x94\x4e\x55"
																	"\x56\x75\xf7\x45\x6e\xa4\x9e\x94"
																	"\xb8\xfe\xda\xd4\xac\x7d\x76\xc5"
																	"\xb4\x65\xed\xb4\x49\x34\x71\x14"
																	"\xdb\x8f\x10\x90\xa3\x05\x02\xdc"
																	"\x86\x92\x6c\xbe\x9b\x57\x32\xe3"
																	"\x2c", 65 },
			0,
		},
		{ 0 }
	};
	unsigned char computation_key_1_key_2_256[] = {237, 79, 25, 206, 175, 8, 68, 40, 211, 211, 89, 243, 
																								48, 185, 245, 148, 186, 7, 192, 195, 65, 128, 247, 36, 
																								232, 44, 220, 134, 248, 120, 54, 178},
								computation_key_1_key_3_256[] = {203, 184, 240, 141, 7, 33, 111, 129, 122, 203, 23, 
																								155, 102, 79, 48, 189, 25, 198, 83, 74, 25, 155, 73, 
																								138, 207, 81, 150, 210, 54, 78, 125, 0},
								computation_key_1_key_2_384[] = {209, 247, 129, 229, 65, 157, 63, 208, 33, 67, 194, 
																								157, 77, 52, 225, 81, 163, 148, 71, 129, 223, 93, 9, 
																								106, 104, 5, 138, 4, 62, 179, 186, 87, 188, 76, 114, 
																								166, 201, 222, 25, 18, 97, 92, 0, 249, 194, 84, 224, 102},
								computation_key_1_key_2_521[] = {0, 200, 130, 8, 33, 155, 171, 165, 118, 212, 254, 220, 
																								207, 115, 244, 1, 12, 62, 32, 21, 88, 225, 231, 89, 73, 
																								197, 210, 34, 42, 183, 168, 220, 24, 52, 112, 158, 36, 
																								49, 181, 55, 96, 108, 19, 3, 209, 79, 103, 122, 71, 134, 
																								106, 48, 125, 240, 36, 186, 134, 141, 204, 115, 122, 18, 
																								63, 230, 94, 45};
	for (int i = 0; test_data[i].curve != 0; i++) {
		gnutls_datum_t x, y, key;

		if (test_data[i].key.data == NULL) {
			genkey(test_data[i].curve, &x, &y, &key);
		} else {
			x = test_data[i].x;
			y = test_data[i].y;
			key = test_data[i].key;
		}

		compute_key(test_data[i].curve, &x, &y, &key,
								&test_data[i].peer_x,
								&test_data[i].peer_y,
								test_data[i].expected_error,
								NULL, 0);

		if (test_data[i].key.data == NULL) {
			gnutls_free(x.data);
			gnutls_free(y.data);
			gnutls_free(key.data);
		}
	}
	
	gnutls_ecdh_compute(private_key_1_ec256, sizeof(private_key_1_ec256), public_key_2_ec256, sizeof(public_key_2_ec256), GNUTLS_E_SUCCESS);
	gnutls_ecdh_compute(private_key_1_ec256, sizeof(private_key_1_ec256), public_key_1_ec384, sizeof(public_key_1_ec384), GNUTLS_E_INVALID_REQUEST);
	gnutls_ecdh_compute(private_key_1_ec256, sizeof(private_key_1_ec256), public_key_1_ec521, sizeof(public_key_1_ec521), GNUTLS_E_INVALID_REQUEST);
	gnutls_ecdh_compute(private_key_1_ec256, sizeof(private_key_1_ec256), public_key_rsa, sizeof(public_key_rsa), GNUTLS_E_INVALID_REQUEST);
	verify_computation(private_key_1_ec256, sizeof(private_key_1_ec256), public_key_2_ec256, sizeof(public_key_2_ec256), computation_key_1_key_2_256, sizeof(computation_key_1_key_2_256));
	verify_computation(private_key_2_ec256, sizeof(private_key_2_ec256), public_key_1_ec256, sizeof(public_key_1_ec256), computation_key_1_key_2_256, sizeof(computation_key_1_key_2_256));
	verify_computation(private_key_1_ec256, sizeof(private_key_1_ec256), public_key_3_ec256, sizeof(public_key_3_ec256), computation_key_1_key_3_256, sizeof(computation_key_1_key_3_256));
	verify_computation(private_key_1_ec384, sizeof(private_key_1_ec384), public_key_2_ec384, sizeof(public_key_2_ec384), computation_key_1_key_2_384, sizeof(computation_key_1_key_2_384));
	verify_computation(private_key_1_ec521, sizeof(private_key_1_ec521), public_key_2_ec521, sizeof(public_key_2_ec521), computation_key_1_key_2_521, sizeof(computation_key_1_key_2_521));

	success("all ok\n");
}
